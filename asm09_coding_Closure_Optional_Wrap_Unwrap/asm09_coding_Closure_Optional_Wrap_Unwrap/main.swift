//
//  main.swift
//  asm09_coding_Closure_Optional_Wrap_Unwrap
//
//  Created by TinhPhan on 6/16/18.
//  Copyright © 2018 TinhPhan. All rights reserved.
//

import Foundation
print(" Khởi tạo và in ra giá trị số nguyên N kiểu Optional")
var N:Int?
N = 32
print(N)
print("Khởi tạo và in ra mảng số nguyên N phần tử kiểu Optional")
var str:String? = "HelloWorld"
print(str)
//
print("Khởi tạo và in ra mảng số nguyên N phần tử kiểu Optional")
var numbers:[Int?] = [5,4,8,nil,345,7]
//for n in 0...5{
//    print(numbers[n])
//}
//
print("Tính tổng hai số X, Y kiểu Optional")
var X:Int? = 6
var Y:Int? = 7
var tong = X!+Y!
// bắt buộc phải unwrap hai giá trị X Y thì mới cộng được
print("Result \(tong)")
print("Viết hàm Closure tính tổng hai số, , gọi và xuất kq")
var tongHaiSo : (Int,Int)->(Int) = {(a:Int,b:Int)->(Int) in a+b}
var result:Int = tongHaiSo(5,5)
print("Result: \(result)")
print("Viết hàm Closure bình phương của một số, , gọi và xuất kq")
var binhPhuongMotSo:(Int)->(Int) = {(a:Int)->(Int) in a*a}
var ketQua = binhPhuongMotSo(3)
print("Result: \(ketQua)")
print("Viết hàm Closure kiểm tra số chia hết cho 5, gọi và xuất kq")
var chiaHetChoNam:(Int)->(String) = {(a:Int)->(String) in
    if(a % 5 == 0){
        return "\(a) chia het cho 5"
    }else{
        return "\(a) khong chia het cho 5"
        
    }
}
print("\(chiaHetChoNam(4))")
print("Viết hàm Closure kiểm tra số chẳn, gọi và xuất kq")
var laSoChan:(Int)->(Bool) = {(a:Int)->(Bool) in a % 2 == 0 ? true : false}
var a:Int = 5
if(laSoChan(a)){
    print("\(a) la so chan")
}else{
    print("\(a)  khong la so chan")
}
print("Viết hàm Closure tạo mảng ngẫu nhiên N phần tử, gọi và xuất kq")
var taoMangNgauNhien:(Int)->[Int] = { (n:Int)->[Int] in
    var numbers: [Int] = []
    for _ in 0..<n{
        numbers.append(Int(arc4random()))
    }
    return numbers
}
var mangNgauNhien = taoMangNgauNhien(10)
print(mangNgauNhien)
print("Viết hàm tìm phần tử nhỏ nhất trong mảng trả về kết quả về bằng Closure")
var timPhanTuNhoNhatTrongMang:([Int])->(Int) = {(mang:[Int]) ->(Int) in
    var min = Int.max
    for i in mang{
        if(i < min){
            min = i
        }
    }
    return min
}
var minMang =  timPhanTuNhoNhatTrongMang(mangNgauNhien)
print(minMang)
print("Viết hàm tìm phần tử chia hết cho 5 trong mảng N phần tử, trả về kết quả bằng Closure")
var timPhanTuChiaHetChoNam:([Int])->([Int]) = {
    (mang:[Int])->([Int])  in
    var mangKq :[Int] = []
    for i in mang{
        if(i % 5 == 0){
            mangKq.append(i)
        }
    }
    return mangKq
}
var mangKetQua = timPhanTuChiaHetChoNam(mangNgauNhien)
print("Ket qua phan tu chia het cho 5 :\(mangKetQua)")
print("Viết hàm tìm phần tử số chẳn đầu tiên trong mảng N phần tử, trả về kết quả bằng Closure")
var phanTuChanDauTien:([Int])->(Int?) = {(mang:[Int])->(Int?) in
    for i in mang{
        if(i % 2 == 0){
            return i
        }
    }
    return nil
}
var ketQuaPhanTuChan:Int? = phanTuChanDauTien(mangNgauNhien)
if(ketQuaPhanTuChan != nil){
    print("Phan tu dau tien cua mang chia het cho 2 la: \(ketQuaPhanTuChan!)")
}else{
    print("Khong co phan tu nao chia het cho 2")
}
