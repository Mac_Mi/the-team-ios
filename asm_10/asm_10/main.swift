//
//  main.swift
//  asm_10
//
//  Created by TinhPhan on 6/18/18.
//  Copyright © 2018 TinhPhan. All rights reserved.
//

import Foundation

print("Tính S(n) = 1 + 1.2 + 1.2.3 + ... + 1.2.3....N")
func tinhNGiaiThua(n:Int)->Int{
    var giaiThua  = 1
    for value in 1...n{
        giaiThua *= value
    }
    return giaiThua
}
var cau1:(Int)->(Int) = {
    (n:Int)->(Int) in
    var sum = 0
    for value in 0..<n{
        sum += tinhNGiaiThua(n: value+1)
    }
    return sum
}
print("Nhap vao so N: ")
var N = Int(readLine(strippingNewline: true)!)
print("Ket qua: \(cau1(N!))")

print("Tính S(n) = x + x^2 + x^3 + ... + x^n")
print("Nhap vao x: ")
var X = Int(readLine(strippingNewline: true)!)

var cau2:(Int,Int)->() = {(x:Int,n:Int)->() in
    var ketqua:Double = 03
    for i in 0..<n{
        ketqua += pow(Double(x), Double(i+1))
    }
    print("Ket qua: \(ketqua)")
}
cau2(X!,N!)

print("Tính S(n) = x^2 + x^4 + ... + x^2n")
var cau3:(Int,Int)->() = { (x:Int,n:Int)->() in
    var ketQua:Double = 0
    for i in 0..<n {
        ketQua += pow(Double(x),Double((2*(i+1))))
    }
    print("Ket qua: \(ketQua)")
}
cau3(X!,N!)
print("Tính S(n) = x + x^3 + x^5 + ... + x^2n + 1")
var cau4:(Int,Int)->() = { (x:Int,n:Int)->() in
    var ketQua:Double = 0
    for i in 0..<n {
        ketQua += pow(Double(x),Double((2*i+1)))
    }
    print("Ket qua: \(ketQua)")
}
cau4(X!,N!)
func tinhTong(n:Int)->(Int){
    var ketQua:Int = 0
    for i in 0...n {
        ketQua += i
    }
    return ketQua
}
print("Tính S(n) = 1 + (1/1) + 2 + (1/ 1 + 2) + (3 + ..... + 1/ 1 + 2 + 3 + .... + N)")
var cau5:(Int)->() = {(n:Int)->() in
    var ketQua:Double = 0
    for i  in 0..<n {
        ketQua += Double(i+1) + (Double(1)/(Double(tinhTong(n: i+1))))
    }
    print("ket qua: \(ketQua)")
}
cau5(N!)
print("Tính S(n) = x + x^2/1 + 2 + x^3/1 + 2 + 3 + ... + x^n/1 + 2 + 3 + .... + N ~ x^N/∑(0..N)")
var cau6:(Int,Int)->() = {(n:Int,x:Int)->() in
    var ketQua:Double = 0
    for i  in 0..<n {
        ketQua += ((pow((Double(x)), Double(i+1)))/Double(tinhTong(n: i+1)))
    }
    print("ket qua: \(ketQua)")
}
cau6(N!,X!)
print("Tính S(n) = x + x^2/2! + x^3/3! + ... + x^n/N!")
var cau7:(Int,Int)->() = {(n:Int,x:Int)->() in
    var ketQua:Double = 0
    for i  in 0..<n {
        ketQua += ((pow((Double(x)), Double(i+1)))/Double(tinhNGiaiThua(n: i+1)))
    }
    print("ket qua: \(ketQua)")
}
cau7(N!,X!)
