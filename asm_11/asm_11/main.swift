//
//  main.swift
//  asm_11
//
//  Created by TinhPhan on 6/18/18.
//  Copyright © 2018 TinhPhan. All rights reserved.
//

import Foundation
print("Viết hàm nhập, xuất mảng 1 chiều các số thực")
func nhapMangMotChieuSoThuc(n:Int)->([Float]){
    var numberArray:[Float] = []
    print("Tong so phan tu: \(n)")
    for i in 0..<n{
        print("Nhap vao phan thu thu \(i+1): ")
        let value =  Float(readLine(strippingNewline: true)!)
        numberArray.insert(value!, at: i)
    }
    return numberArray
}
func xuatMangSoThuc(mang:[Float]){
    print(mang)
}
//xuatMangSoThuc(mang: nhapMangMotChieuSoThuc(n: 3))
print("Viết hàm nhập, xuất mảng 1 chiều các số nguyên")
func nhapMangMotChieuSoNguyen(n:Int)->([Int]){
    var numberArray:[Int] = []
    print("Tong so phan tu: \(n)")
    for i in 0..<n{
        print("Nhap vao phan thu thu \(i+1): ")
        let value =  Int(readLine(strippingNewline: true)!)
        numberArray.insert(value!, at: i)
    }
    return numberArray
}
func xuatMangSoNguyen(mang:[Int]){
    print(mang)
}
//xuatMangSoNguyen(mang: nhapMangMotChieuSoNguyen(n: 3))
print("Viết hàm liệt kê các giá trị chẵn trong mảng 1 chiều các số nguyên")
func taoMangSoNguyenNgauNhien(n:Int)->([Int]){
    var numbers:[Int] = []
    for i in 0..<n{
        numbers.insert(Int(arc4random()), at: i)
    }
    return numbers
}
func lietKeCacGiaTriChan(mang:[Int])->([Int]){
    var mangKetQua:[Int]=[]
    for i in mang{
        if(i % 2 == 0 ){
            mangKetQua.append(i)
        }
    }
    return mangKetQua
}
var mangSoNguyen = taoMangSoNguyenNgauNhien(n: 10)
//var mangSoNguyen = nhapMangMotChieuSoNguyen(n: 10)

print("Mang hien hanh: \(mangSoNguyen)")
if(lietKeCacGiaTriChan(mang: mangSoNguyen).count == 0){
    print("Khong co gia tri nao la so chan")
}else{
    print("Cac so chan trong mang so nguyen: \(lietKeCacGiaTriChan(mang: mangSoNguyen))")
}
print("Viết hàm liệt kê các vị trí mà giá trị tại đó là giá trị âm trong mảng 1 chiều các số thực")
func taoMangSoThucNgauNhien(n:Int)->([Float]){
    var numbers:[Float] = []
    for i in 0..<n{
        numbers.insert(Float(arc4random()), at: i)
    }
    return numbers
}
func lietKeViTriGiaTriAm(mang:[Float])->[Int]{
    var indexs:[Int] = []
    for i in 0..<mang.count{
        if(mang[i] < 0){
            indexs.append(i)
        }
    }
    return indexs
}
var mangSoThuc = taoMangSoThucNgauNhien(n: 10)
//var mangSoThuc = nhapMangMotChieuSoThuc(n: 3)

print("Mang so thuc hien hanh: \(mangSoThuc)")
if(lietKeViTriGiaTriAm(mang: mangSoThuc).count == 0){
    print("Khong co gia tri nao la am")
}else{
    print("Cac vi tri am trong mang so thuc: \(lietKeViTriGiaTriAm(mang: mangSoThuc))")
}
print("Viết hàm tìm giá trị lớn nhất trong mảng 1 chiều các số thực")

func timMaxMang(mang:[Float])->(Float){
    var max = -1*(Float.infinity)
    for value in mang {
        if(value > max){
            max = value
        }
    }
    return max
}
print("Gia tri lon nhat cua mang so thuc la \(timMaxMang(mang: mangSoThuc))")

print("Viết hàm tìm giá trị dương đầu tiên trong mảng 1 chiều các số thực. Nếu mảng không có giá trị dương thì trả về -1")
func timGiaTriDuongDauTienTrongMang(mang:[Float])->(Float){
    for value in mang{
        if(value >= 0){
            return value
        }
    }
    return -1
}
if(timGiaTriDuongDauTienTrongMang(mang: mangSoThuc) >= 0){
    print("Gia tri duong dau tien trong mang so thuc la: \(timGiaTriDuongDauTienTrongMang(mang: mangSoThuc))")
}else{
    print("Khong co gia tri duong nao")
}
print("Tìm số chẵn cuối cùng trong mảng 1 chiều các số nguyên. Nếu mảng không có giá trị chẵn thì trả về -1")
print("Mang so nguyen hien hanh: \(mangSoNguyen)")
func timGiaTriChanCuoiCungTrongMangSoNguyen (mang:[Int])->(Int){
    for i in mang.reversed(){
        if(i % 2 == 0){
            return i
        }
    }
    return -1
}
if(timGiaTriChanCuoiCungTrongMangSoNguyen(mang: mangSoNguyen) == -1){
    print("Khong co gia tri nao la chan")
}else{
    print("Gia tri chan cuoi cung trong mang la: \(timGiaTriChanCuoiCungTrongMangSoNguyen(mang: mangSoNguyen))")
}
print("Tìm vị trí của giá trị chẵn đầu tiên trong mảng 1 chiều các số nguyên. Nếu mảng không có giá trị chẵn thì sẽ trả về -1")
func timGiaTriChanDauTienTrongMangSoNguyen(mang:[Int])->(Int){
    for i in mang{
        if(i % 2 == 0){
            return i
        }
    }
    return -1
}
if(timGiaTriChanDauTienTrongMangSoNguyen(mang: mangSoNguyen) == -1){
    print("Khong co gia tri nao la chan")
}else{
    print("Gia tri chan dai tien trong mang la: \(timGiaTriChanDauTienTrongMangSoNguyen(mang: mangSoNguyen))")
}
print("Tìm vị trí số hoàn thiện cuối cùng trong mảng 1 chiều các số nguyên. Nếu mảng không có số hoàn thiện thì trả về giá trị -1")
func kiemTraSoHoanThien(n:Int)->(Bool){
    var tongUocSo = 0
    for i in 1..<n{
        if(n % i == 0){
            tongUocSo += i
        }
    }
    return tongUocSo == n ? true : false
}
func timSoHoanThienCuoiCung(mang:[Int])->(Int){
    for i in mang.reversed(){
        if(kiemTraSoHoanThien(n: i)){
            return mang.index(of: i)!
        }
    }
    return -1
}
var mangTest = [9,2,6,28,9]
var ketQua = timSoHoanThienCuoiCung(mang: mangTest)
if(ketQua != -1){
    print("Ket qua: \(ketQua)")
}else{
    print("Khong co so hoan thien nao")
}
print("Tìm 1 vị trí mà giá trị tại vị trí đó là giá trị nhỏ nhất trong mảng 1 chiều các số thực")
print("Mang so thuc hien hanh: \(mangSoThuc)")
func timViTriGiaTriMinTrongMangSoThuc(mang:[Float])->(Int){
    var index = 0
    var min = Float.infinity
    for value in mang{
        if(value < min ){
            min = value
            index =  mang.index(of: value)!
        }
    }
    return index
}
print("Ket qua: \(timViTriGiaTriMinTrongMangSoThuc(mang: mangSoThuc))")

print("Hãy tìm giá trị dương nhỏ nhất trong mảng 1 chiều các số thực. Nếu mảng không có giá trị dương thì sẽ trả về -1")
func timGiaTriDuongNhoNhatMangSoThuc(mang:[Float])->(Float){
    var min = Float.infinity
    for value in mang{
        if(value >= 0 && value < min){
            min  = value
        }
    }
    return min == Float.infinity ? -1: min
}
if(timGiaTriDuongNhoNhatMangSoThuc(mang: mangSoThuc) == -1){
    print("Mang khong co gia tri duong nao")
}else{
    print("Ket qua: \(timGiaTriDuongNhoNhatMangSoThuc(mang: mangSoThuc))")
}
print("Hãy tìm vị trí giá trị dương nhỏ nhất trong mảng 1 chiều các số thực. Nếu mảng không có giá trị dương thì trả về -1")
func timViTriGiaTriDuongNhoNhatMangSoThuc(mang:[Float])->(Int){
    var min = Float.infinity
    var index = -1
    for value in mang{
        if(value >= 0 && value  < min){
            min  = value
            index = mang.index(of: value)!
        }
    }
    return index
}
if(timViTriGiaTriDuongNhoNhatMangSoThuc(mang: mangSoThuc) == -1){
    print("Khong co gia tri duong nao")
}else{
    print("vi tri: \(timViTriGiaTriDuongNhoNhatMangSoThuc(mang: mangSoThuc))" )
}

