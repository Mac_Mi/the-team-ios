//
//  Program.swift
//  asm07_coding_protocol
//
//  Created by TinhPhan on 6/15/18.
//  Copyright © 2018 TinhPhan. All rights reserved.
//

import Foundation
class Program {
    var mTenPhanMem:String
    var mNhaPhatHanh:String
    var mPhienBan:String
    var mGiaTien:Float
    init(tenPhanMem:String, nhaPhatHanh:String,phienBan:String,giaTien:Float) {
        self.mTenPhanMem = tenPhanMem
        self.mNhaPhatHanh = nhaPhatHanh
        self.mPhienBan = phienBan
        self.mGiaTien = giaTien
    }
    func xuatThongTin()  {
        print("Ten phan mem: \(mTenPhanMem)")
        print("Nha phat hanh: \(mNhaPhatHanh)")
        print("Phien ban: \(mPhienBan)")
        print("Gia tien: \(mGiaTien)")
    }
}
