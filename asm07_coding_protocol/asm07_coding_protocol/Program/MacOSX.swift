//
//  MacOSX.swift
//  asm07_coding_protocol
//
//  Created by TinhPhan on 6/15/18.
//  Copyright © 2018 TinhPhan. All rights reserved.
//

import Foundation
protocol MacOSX{
    var mDuoiMoRong:String {get}
    var mDoTuongThich:String{get set}    
}
