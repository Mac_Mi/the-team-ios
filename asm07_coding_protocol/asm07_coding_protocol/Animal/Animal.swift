//
//  Animal.swift
//  asm07_coding_protocol
//
//  Created by TinhPhan on 6/15/18.
//  Copyright © 2018 TinhPhan. All rights reserved.
//

import Foundation
class Animal {
    var mTen:String
    var mSoChan:Int
    init(ten:String,soChan:Int) {
        self.mTen = ten
        self.mSoChan = soChan
    }
    func hienThiThongTin()  {
        print("Ten \(mTen) , so chan \(mSoChan)")
    }
}
