//
//  Bird.swift
//  asm07_coding_protocol
//
//  Created by TinhPhan on 6/15/18.
//  Copyright © 2018 TinhPhan. All rights reserved.
//

import Foundation
class Bird: Animal,TheVoid,Fly {
    override init(ten: String, soChan: Int) {
        super .init(ten: ten, soChan: soChan)
    }
    func theVoid() {
        print("Liu lo liu lo")
    }
    func fly() {
        print("Can fly")
    }
}
