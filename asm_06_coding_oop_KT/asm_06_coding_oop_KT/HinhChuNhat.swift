//
//  HinhChuNhat.swift
//  asm_06_coding_oop_KT
//
//  Created by TinhPhan on 6/15/18.
//  Copyright © 2018 TinhPhan. All rights reserved.
//

import Foundation
class HinhChuNhat: HinhHoc {
    private var mDai:Double
    private var mRong:Double
    init(dai:Double,rong:Double) {
        self.mDai = dai;
        self.mRong = rong
    }
    public override func tinhChuVi() -> Double {
        return (mDai + mRong) * 2
    }
    public  override func tinhDienTich() -> Double {
        return mDai * mRong
    }
    public func exportInfo()->String{
        return "Hinh Chu Nhat"
    }
}
