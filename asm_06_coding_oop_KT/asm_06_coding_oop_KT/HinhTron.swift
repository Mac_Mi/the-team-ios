//
//  HinhTron.swift
//  asm_06_coding_oop_KT
//
//  Created by TinhPhan on 6/15/18.
//  Copyright © 2018 TinhPhan. All rights reserved.
//

import Foundation
class HinhTron: HinhHoc {
    private var mBanKinh:Double
    init(banKinh:Double) {
        self.mBanKinh = banKinh
    }
    public override func tinhChuVi() -> Double {
        return mBanKinh * 2 * Double.pi
    }
    public  override func tinhDienTich() -> Double {
        return mBanKinh * mBanKinh * Double.pi
    }
    public func exportInfo()->String{
        return "Hinh Tron"
    }
}
