//
//  main.swift
//  asm_06_coding_oop_KT
//
//  Created by TinhPhan on 6/15/18.
//  Copyright © 2018 TinhPhan. All rights reserved.
//

import Foundation

var hinhTron = HinhTron(banKinh: 9)
hinhTron.hienThiThongTin(thongTin: hinhTron.exportInfo())
var hinhVuong = HinhVuong(canh: 5.6)
hinhVuong.hienThiThongTin(thongTin:hinhVuong.exportInfo())
var hinhChuNhat = HinhChuNhat(dai: 5, rong: 7)
hinhChuNhat.hienThiThongTin(thongTin: hinhChuNhat.exportInfo())

