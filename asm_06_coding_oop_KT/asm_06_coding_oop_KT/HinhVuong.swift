//
//  HinhVuong.swift
//  asm_06_coding_oop_KT
//
//  Created by TinhPhan on 6/15/18.
//  Copyright © 2018 TinhPhan. All rights reserved.
//

import Foundation
class HinhVuong: HinhHoc {
    private var mCanh:Double
    init(canh:Double) {
        self.mCanh=canh
    }
    public override func tinhChuVi() -> Double {
        return mCanh*4
    }
  public  override func tinhDienTich() -> Double {
        return mCanh*mCanh
    }
    public func exportInfo()->String{
        return "Hinh Vuong"
    }
}
