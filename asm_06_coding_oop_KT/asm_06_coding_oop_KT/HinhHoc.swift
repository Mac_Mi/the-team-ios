//
//  HinhHoc.swift
//  asm_06_coding_oop_KT
//
//  Created by TinhPhan on 6/15/18.
//  Copyright © 2018 TinhPhan. All rights reserved.
//

import Foundation
class HinhHoc{
    public func  tinhChuVi()->Double{
        return 0
    }
    public func tinhDienTich()->Double{
        return 0
    }
    public func hienThiThongTin(thongTin:String){
        print("\(thongTin) co dien tich la: \(tinhDienTich()) va chu vi la: \(tinhChuVi())")
    }
}
