//
//  ViewController.swift
//  demo_layout_clean_model_coding
//
//  Created by TinhPhan on 6/12/18.
//  Copyright © 2018 TinhPhan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupBackground()
        setupSubViewBlue()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        setRatioAndHeightConstraint(sub: mMenuBlueHorizontal, superView: mBackgroundBlueView)
        setRatioAndHeightConstraint(sub: mMenuPoint, superView: mBackgroundBlueView)
        setRatioAndHeightConstraint(sub: mBatteryImage, superView: mBackgroundBlueView)
        setupSubViewPinkView()
    }
    //region UTILS
    func setupBackground(){
        self.view.addSubview(mBackgroundBlueView)
        self.view.addSubview(mBackgroundPinkView)
        mBackgroundPinkView.translatesAutoresizingMaskIntoConstraints = false
        mBackgroundBlueView.translatesAutoresizingMaskIntoConstraints = false
                mBackgroundBlueView.backgroundColor = UIColor(rgb: 0x4A8DC1)
                mBackgroundPinkView.backgroundColor = UIColor(rgb: 0xEFF0F2)
        setupBackgroundConstrain()
    }
    func setupBackgroundConstrain() {
        // constrain bg blu
        let topBlu = NSLayoutConstraint(item: mBackgroundBlueView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0)
        let leadingBlu = NSLayoutConstraint(item: mBackgroundBlueView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
        let trailingBlu = NSLayoutConstraint(item: mBackgroundBlueView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
        let heightBlu = NSLayoutConstraint(item: mBackgroundBlueView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: self.view.bounds.size.height*(1/3))
        
        heightBlu.isActive = true
        topBlu.isActive = true
        leadingBlu.isActive = true
        trailingBlu.isActive = true
        // constrain bg pin
        let leadingPink = NSLayoutConstraint(item: mBackgroundPinkView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
        let trailingPink = NSLayoutConstraint(item: mBackgroundPinkView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
        let heightPink = NSLayoutConstraint(item: mBackgroundPinkView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: self.view.bounds.size.height*(2/3))
        let bottomPink = NSLayoutConstraint(item: mBackgroundPinkView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
        
        leadingPink.isActive = true
        trailingPink.isActive =  true
        heightPink.isActive = true
        bottomPink.isActive = true
    }
    // setup sub view in view blue
    func setupMenuHorizontal() {
        setupCommonImage(imageSrc: mMenuBlueHorizontal, name: "menu.png", superView: mBackgroundBlueView)
        let topMenu = NSLayoutConstraint(item: mMenuBlueHorizontal, attribute: .top, relatedBy: .equal, toItem: mBackgroundBlueView, attribute: .top, multiplier: 1, constant: 32)
        let leadingMenu = NSLayoutConstraint(item: mMenuBlueHorizontal, attribute: .leading, relatedBy: .equal, toItem: mBackgroundBlueView, attribute: .leading, multiplier: 1, constant: 20)
        topMenu.isActive = true
        leadingMenu.isActive = true
    }
    func setupNameApp() {
        setupTextCommon(sub: mNameApp, font: 30.0, text: "DEEBOT N79", superView: mBackgroundBlueView, color: UIColor.white)
        let topName = NSLayoutConstraint(item: mNameApp, attribute: .top, relatedBy: .equal, toItem: mBackgroundBlueView, attribute: .top, multiplier: 1, constant: 32)
        let horizonotalCenterName = NSLayoutConstraint(item: mNameApp, attribute: .centerX, relatedBy: .equal, toItem: mBackgroundBlueView, attribute: .centerX, multiplier: 1, constant: 0)
        topName.isActive = true
        horizonotalCenterName.isActive = true
        
    }
    func setupMenuPoint() {
        setupCommonImage(imageSrc: mMenuPoint, name: "point.png", superView: mBackgroundBlueView)
        let topMenu = NSLayoutConstraint(item: mMenuPoint, attribute: .top, relatedBy: .equal, toItem: mBackgroundBlueView, attribute: .top, multiplier: 1, constant: 32)
        let trailingMenu = NSLayoutConstraint(item: mMenuPoint, attribute: .trailing, relatedBy: .equal, toItem: mBackgroundBlueView, attribute: .trailing, multiplier: 1, constant: -20)
        topMenu.isActive = true
        trailingMenu.isActive = true
    }
    func setupVerticalCenterTop(){
        let verticalOne = NSLayoutConstraint(item: mMenuBlueHorizontal, attribute: .centerY, relatedBy: .equal, toItem: mNameApp, attribute: .centerY, multiplier: 1, constant: 0)
        let verticalTwo = NSLayoutConstraint(item: mMenuBlueHorizontal, attribute: .centerY, relatedBy: .equal, toItem: mMenuPoint, attribute: .centerY, multiplier: 1, constant: 0)
        verticalOne.isActive = true
        verticalTwo.isActive = true
    }
    func setupTextStandby()  {
        setupTextCommon(sub: mTextStandby, font: 30.0, text: "Standby", superView: mBackgroundBlueView, color: UIColor.white)
        let vertical = NSLayoutConstraint(item: mTextStandby, attribute: .centerY, relatedBy: .equal, toItem: mBackgroundBlueView, attribute: .centerY, multiplier: 1, constant: 0)
        let horizontal = NSLayoutConstraint(item: mTextStandby, attribute: .centerX, relatedBy: .equal, toItem: mBackgroundBlueView, attribute: .centerX, multiplier: 0.6, constant: 0)
        vertical.isActive =  true
        horizontal.isActive = true
        
    }
    func setupImageBattery() {
        setupCommonImage(imageSrc: mBatteryImage, name: "battery.png", superView: mBackgroundBlueView)
        let vertical = NSLayoutConstraint(item: mBatteryImage, attribute: .centerY, relatedBy: .equal, toItem: mBackgroundBlueView, attribute: .centerY, multiplier: 1, constant: 0)
        let horizontal = NSLayoutConstraint(item: mBatteryImage, attribute: .centerX, relatedBy: .equal, toItem: mBackgroundBlueView, attribute: .centerX, multiplier: 1.4, constant: 0)
        let top = NSLayoutConstraint(item: mBatteryImage, attribute: .top, relatedBy: .equal, toItem: mNameApp, attribute: .top, multiplier: 1, constant: 64)
        let ratio = NSLayoutConstraint(item: mBatteryImage, attribute: .height, relatedBy: .equal, toItem: mBatteryImage, attribute: .width, multiplier: 1, constant: 1)
        let height = NSLayoutConstraint(item: mBatteryImage, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: self.view.bounds.size.height/10)
        height.isActive = true
        ratio.isActive = true
        top.isActive = true
        vertical.isActive =  true
        horizontal.isActive = true
    }
    func setupTextStatus() {
        setupTextCommon(sub: mTextStatus, font: 20, text: "status", superView: mBackgroundBlueView, color: UIColor.white)
        let top = NSLayoutConstraint(item: mTextStatus, attribute: .top, relatedBy: .equal, toItem: mTextStandby, attribute: .bottom, multiplier: 1, constant: 8)
        let horizontal = NSLayoutConstraint(item: mTextStatus, attribute: .centerX, relatedBy: .equal, toItem: mTextStandby, attribute: .centerX, multiplier: 1, constant: 0)
        top.isActive = true
        horizontal.isActive = true
    }
    func setupTextBattery() {
        setupTextCommon(sub: mTextBattery, font: 20, text: "battrey", superView: mBackgroundBlueView, color: UIColor.white)
        let top = NSLayoutConstraint(item: mTextBattery, attribute: .top, relatedBy: .equal, toItem: mBatteryImage, attribute: .bottom, multiplier: 1, constant: 8)
        let horizontal = NSLayoutConstraint(item: mTextBattery, attribute: .centerX, relatedBy: .equal, toItem: mBatteryImage, attribute: .centerX, multiplier: 1, constant: 0)
        top.isActive = true
        horizontal.isActive = true
    }
    func setupVerticalCenterForTextDescription() {
        let verticalOne = NSLayoutConstraint(item: mTextStatus, attribute: .centerY, relatedBy: .equal, toItem: mTextBattery, attribute: .centerY, multiplier: 1, constant: 0)
        verticalOne.isActive = true
    }
    //end setup
    
    //utils
    func setupTextCommon(sub: UILabel, font: CGFloat, text:String,superView:UIView,color:UIColor) {
        superView.addSubview(sub)
        sub.translatesAutoresizingMaskIntoConstraints =  false
        sub.text = text
        sub.font = UIFont.systemFont(ofSize: font)
        sub.textColor = color
    }
    func setupCommonImage(imageSrc: UIImageView, name:String,superView:UIView) {
        superView.addSubview(imageSrc)
        let image = UIImage(named:name)
        imageSrc.image=image;
        imageSrc.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func setRatioAndHeightConstraint(sub: UIImageView,superView : UIView ) {
        let ratio = NSLayoutConstraint(item: sub, attribute: .height, relatedBy: .equal, toItem: sub, attribute: .width, multiplier: 1, constant: 1)
        let height =  NSLayoutConstraint(item: sub, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: superView.bounds.size.height/6)
        height.isActive = true
        ratio.isActive =  true
    }
    func setupSubViewBlue()  {
        setupMenuHorizontal()
        setupNameApp()
        setupMenuPoint()
        setupVerticalCenterTop()
        setupTextStandby()
        setupImageBattery()
        setupTextStatus()
        setupTextBattery()
        setupVerticalCenterForTextDescription()
    }
    //end
    // set up view pink
    func setupSubViewBigInPinkView() {
        mBackgroundPinkView.addSubview(mSubBgBigPinkView)
        mSubBgBigPinkView.translatesAutoresizingMaskIntoConstraints = false
        let top = NSLayoutConstraint(item: mSubBgBigPinkView, attribute: .top, relatedBy: .equal, toItem: mBackgroundPinkView, attribute: .top, multiplier: 1, constant: 0)
        let leading = NSLayoutConstraint(item: mSubBgBigPinkView, attribute: .leading, relatedBy: .equal, toItem:mBackgroundPinkView, attribute: .leading, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: mSubBgBigPinkView, attribute: .trailing, relatedBy: .equal, toItem: mBackgroundPinkView, attribute: .trailing, multiplier: 1, constant: 0)
        let height = NSLayoutConstraint(item: mSubBgBigPinkView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: mBackgroundPinkView.bounds.size.height*(3/4))
        height.isActive = true
        top.isActive = true
        leading.isActive = true
        trailing.isActive = true
    }
    
    func setupSubViewSmallInPinkView() {
        mBackgroundPinkView.addSubview(mSubBgSmallPinkView)
        mSubBgSmallPinkView.translatesAutoresizingMaskIntoConstraints = false
        let bottom = NSLayoutConstraint(item: mSubBgSmallPinkView, attribute: .bottom, relatedBy: .equal, toItem: mBackgroundPinkView, attribute: .bottom, multiplier: 1, constant: 0)
        let leading = NSLayoutConstraint(item: mSubBgSmallPinkView, attribute: .leading, relatedBy: .equal, toItem: mBackgroundPinkView, attribute: .leading, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: mSubBgSmallPinkView, attribute: .trailing, relatedBy: .equal, toItem: mBackgroundPinkView, attribute: .trailing, multiplier: 1, constant: 0)
        let height = NSLayoutConstraint(item: mSubBgSmallPinkView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: mBackgroundPinkView.bounds.size.height*(1/4))
        height.isActive = true
        bottom.isActive = true
        leading.isActive = true
        trailing.isActive = true
    }
    func setupCircleView() {
        mSubBgBigPinkView.addSubview(mCircleView)
        mCircleView.translatesAutoresizingMaskIntoConstraints = false
                mCircleView.backgroundColor = UIColor(rgb: 0xF5F6FA)
        mCircleView.frame = CGRect(x:0,y:0,width:self.view.bounds.size.height*(1/3),height:self.view.bounds.size.height*(1/3))
        let vertical = NSLayoutConstraint(item: mCircleView, attribute: .centerY, relatedBy: .equal, toItem:mSubBgBigPinkView , attribute: .centerY, multiplier: 1, constant: 0)
        let horizontal = NSLayoutConstraint(item: mCircleView, attribute: .centerX, relatedBy: .equal, toItem: mSubBgBigPinkView, attribute: .centerX, multiplier: 1, constant: 0)
        let ratio = NSLayoutConstraint(item: mCircleView, attribute: .height, relatedBy: .equal, toItem: mCircleView, attribute: .width, multiplier: 1, constant: 1)
        let height = NSLayoutConstraint(item: mCircleView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: self.view.bounds.size.height*(1/3))
        vertical.isActive = true
        horizontal.isActive = true
        ratio.isActive = true
        height.isActive = true
        mCircleView.layer.cornerRadius = mCircleView.bounds.size.height/2
        mCircleView.clipsToBounds = true
        mCircleView.layer.masksToBounds = true
        mCircleView.layer.shadowColor = UIColor.blue.cgColor
        mCircleView.layer.shadowOpacity = 0.5
        
    }
    func setupImageArrowUp() {
        setupCommonImage(imageSrc: mImageArrowUp, name: "top.png", superView: mCircleView)
        let top = NSLayoutConstraint(item: mImageArrowUp, attribute: .top, relatedBy: .equal, toItem: mCircleView, attribute: .top, multiplier: 1, constant: 32)
        let horizontal = NSLayoutConstraint(item: mImageArrowUp, attribute: .centerX, relatedBy: .equal, toItem: mCircleView, attribute: .centerX, multiplier: 1, constant: 0)
        setRatioAndHeightConstraint(sub: mImageArrowUp, superView: mCircleView)
        top.isActive = true
        horizontal.isActive = true
    }
    func setupImageArrowLeft() {
        setupCommonImage(imageSrc: mImageArrowLeft, name: "left.png", superView: mCircleView)
        let leading = NSLayoutConstraint(item: mImageArrowLeft, attribute: .leading, relatedBy: .equal, toItem: mCircleView, attribute: .leading, multiplier: 1, constant: 32)
        let vertical = NSLayoutConstraint(item: mImageArrowLeft, attribute: .centerY, relatedBy: .equal, toItem: mCircleView, attribute: .centerY, multiplier: 1, constant: 0)
        setRatioAndHeightConstraint(sub: mImageArrowLeft, superView: mCircleView)
        leading.isActive = true
        vertical.isActive = true
    }
    func setupImageArrowRight() {
        setupCommonImage(imageSrc: mImageArrowRight, name: "right.png", superView: mCircleView)
        let trailing = NSLayoutConstraint(item: mImageArrowRight, attribute: .trailing, relatedBy: .equal, toItem: mCircleView, attribute: .trailing, multiplier: 1, constant: -32)
        let vertical = NSLayoutConstraint(item: mImageArrowRight, attribute: .centerY, relatedBy: .equal, toItem: mCircleView, attribute: .centerY, multiplier: 1, constant: 0)
        setRatioAndHeightConstraint(sub: mImageArrowRight, superView: mCircleView)
        trailing.isActive = true
        vertical.isActive = true
    }
    func setupImageArrowDown() {
        setupCommonImage(imageSrc: mImageArrowDown, name: "down.png", superView: mCircleView)
        let bottom = NSLayoutConstraint(item: mImageArrowDown, attribute: .bottom, relatedBy: .equal, toItem: mCircleView, attribute: .bottom, multiplier: 1, constant: -32)
        let horizontal = NSLayoutConstraint(item: mImageArrowDown, attribute: .centerX, relatedBy: .equal, toItem: mCircleView, attribute: .centerX, multiplier: 1, constant: 0)
        setRatioAndHeightConstraint(sub: mImageArrowDown, superView: mCircleView)
        bottom.isActive = true
        horizontal.isActive = true
    }
    func setupImageAutoAction() {
        setupCommonImage(imageSrc: mImageAuto, name: "international.png", superView: mSubBgSmallPinkView)
        let leading = NSLayoutConstraint(item: mImageAuto, attribute: .leading, relatedBy: .equal, toItem: mSubBgBigPinkView, attribute: .leading, multiplier: 1, constant: 20)
        let vertical = NSLayoutConstraint(item: mImageAuto, attribute: .centerY, relatedBy: .equal, toItem: mSubBgSmallPinkView, attribute: .centerY, multiplier: 1, constant: 0)
        leading.isActive = true
        vertical.isActive = true
        setRatioAndWidthConstraint(sub: mImageAuto, superView: mSubBgSmallPinkView)
        
    }
    func setupTextAuto() {
        setupTextCommon(sub: mTextAuto, font: 17, text: "AUTO", superView: mSubBgBigPinkView, color: UIColor.gray)
        setRectTextConstraintBelowImage(label: mTextAuto, image: mImageAuto)
        
    }
    func setupImageChargeAction() {
        setupCommonImage(imageSrc: mImageCharge, name: "share.png", superView: mSubBgSmallPinkView)
        let trailing = NSLayoutConstraint(item: mImageCharge, attribute: .trailing, relatedBy: .equal, toItem: mSubBgSmallPinkView, attribute: .trailing, multiplier: 1, constant: -20)
        let vertical = NSLayoutConstraint(item: mImageCharge, attribute: .centerY, relatedBy: .equal, toItem: mSubBgSmallPinkView, attribute: .centerY, multiplier: 1, constant: 0)
        setRatioAndWidthConstraint(sub: mImageCharge, superView: mSubBgBigPinkView)
        trailing.isActive = true
        vertical.isActive = true
        
    }
    func setupTextCharge() {
        setupTextCommon(sub: mTextCharge, font: 17, text: "Charge", superView: mSubBgBigPinkView, color: UIColor.gray)
        setRectTextConstraintBelowImage(label: mTextCharge, image: mImageCharge)
        
    }
    func setupImageSpotAction() {
        setupCommonImage(imageSrc: mImageSpot, name: "diagonal-enlarge", superView: mSubBgSmallPinkView)
        let horizontal = NSLayoutConstraint(item: mImageSpot, attribute: .centerX, relatedBy: .equal, toItem: mSubBgSmallPinkView, attribute: .centerX, multiplier: 1, constant: 0)
        let vertical = NSLayoutConstraint(item: mImageSpot, attribute: .centerY, relatedBy: .equal, toItem: mSubBgSmallPinkView, attribute: .centerY, multiplier: 1, constant: 0)
        vertical.isActive = true
        horizontal.isActive = true
        setRatioAndWidthConstraint(sub: mImageSpot, superView: mSubBgSmallPinkView)
    }
    func setupTextSpot()  {
        setupTextCommon(sub: mTextSpot, font: 17, text: "Spot", superView: mSubBgSmallPinkView, color: UIColor.gray)
        setRectTextConstraintBelowImage(label: mTextSpot, image: mImageSpot)
        
    }
    func setupImageEdgeAction()  {
        setupCommonImage(imageSrc: mImageEdge, name: "close.png", superView: mSubBgSmallPinkView)
        setRatioAndWidthConstraint(sub: mImageEdge, superView: mSubBgSmallPinkView)
        let horizontal = NSLayoutConstraint(item: mImageEdge, attribute: .centerX, relatedBy: .equal, toItem: mSubBgSmallPinkView, attribute: .centerX, multiplier: 0.6, constant: 0)
        let vertical = NSLayoutConstraint(item: mImageEdge, attribute: .centerY, relatedBy: .equal, toItem: mSubBgSmallPinkView, attribute: .centerY, multiplier: 1, constant: 0)
        vertical.isActive = true
        horizontal.isActive = true
        
    }
    func setupTextEdge()  {
        setupTextCommon(sub: mTextEdge, font: 17, text: "Edge", superView: mSubBgSmallPinkView, color: UIColor.gray)
        setRectTextConstraintBelowImage(label: mTextEdge, image: mImageEdge)
    }
    func setupImageRoomAction() {
        setupCommonImage(imageSrc: mImageRoom, name: "user.png", superView: mSubBgSmallPinkView)
        setRatioAndWidthConstraint(sub: mImageRoom, superView: mSubBgSmallPinkView)
        let horizontal = NSLayoutConstraint(item: mImageRoom, attribute: .centerX, relatedBy: .equal, toItem: mSubBgSmallPinkView, attribute: .centerX, multiplier: 1.4, constant: 0)
        let vertical = NSLayoutConstraint(item: mImageRoom, attribute: .centerY, relatedBy: .equal, toItem: mSubBgSmallPinkView, attribute: .centerY, multiplier: 1, constant: 0)
        vertical.isActive = true
        horizontal.isActive = true
    }
    func setupTextRoom()  {
        setupTextCommon(sub: mTextRoom, font: 17, text: "Room", superView: mSubBgSmallPinkView, color: UIColor.gray)
        setRectTextConstraintBelowImage(label: mTextRoom, image: mImageRoom)
    }
    func setRectTextConstraintBelowImage(label:UILabel,image:UIImageView) {
        let top = NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: image, attribute: .bottom, multiplier: 1, constant: 8)
        let horizontal = NSLayoutConstraint(item: label, attribute: .centerX, relatedBy: .equal, toItem: image, attribute: .centerX, multiplier: 1, constant: 0)
        top.isActive = true
        horizontal.isActive = true
    }
    func setRatioAndWidthConstraint(sub: UIImageView,superView : UIView ) {
        let ratio = NSLayoutConstraint(item: sub, attribute: .height, relatedBy: .equal, toItem: sub, attribute: .width, multiplier: 1, constant: 1)
        let width =  NSLayoutConstraint(item: sub, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: self.view.bounds.size.width/8)
        width.isActive = true
        ratio.isActive =  true
    }
    func setupSubViewPinkView()  {
        setupSubViewBigInPinkView()
        setupSubViewSmallInPinkView()
        setupCircleView()
        setupImageArrowUp()
        setupImageArrowLeft()
        setupImageArrowRight()
        setupImageArrowDown()
        setupImageAutoAction()
        setupTextAuto()
        setupImageChargeAction()
        setupTextCharge()
        setupImageSpotAction()
        setupTextSpot()
        setupImageEdgeAction()
        setupTextEdge()
        setupImageRoomAction()
        setupTextRoom()
    }
    
    //endsetup
    
    //endregion
    
    //region VARS
    let mBackgroundBlueView = UIView()
    let mBackgroundPinkView = UIView()
    let mMenuBlueHorizontal = UIImageView()
    let mNameApp = UILabel()
    let mMenuPoint = UIImageView()
    let mTextStandby = UILabel()
    let mBatteryImage = UIImageView()
    let mTextStatus = UILabel()
    let mTextBattery = UILabel()
    let mSubBgSmallPinkView = UIView()
    let mSubBgBigPinkView = UIView()
    let mCircleView = UIView()
    let mImageArrowUp = UIImageView()
    let mImageArrowLeft = UIImageView()
    let mImageArrowRight = UIImageView()
    let mImageArrowDown = UIImageView()
    let mImageAuto = UIImageView()
    let mTextAuto = UILabel()
    let mImageCharge = UIImageView()
    let mTextCharge = UILabel()
    let mImageSpot = UIImageView()
    let mTextSpot = UILabel()
    let mImageEdge = UIImageView()
    let mTextEdge = UILabel()
    let mImageRoom = UIImageView()
    let mTextRoom = UILabel()
    //endregion
}
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}
