//
//  ViewController.swift
//  ams_01_by_coding
//
//  Created by TinhPhan on 6/13/18.
//  Copyright © 2018 TinhPhan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupImageView()
        setupImageLogo()
        setupMidView()
        setupLabelName()
        setupLabelDesName()
        setupLabelClass()
        setupLabelId()
        setupLableAddress()
        setupLabelDesClass()
        setupLabelDesID()
        setupLabelDesAddress()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setupImageViewCommon(imageViewSub:UIImageView,namePicture:String) {
        let image = UIImage(named: namePicture)
        imageViewSub.image=image
        imageViewSub.translatesAutoresizingMaskIntoConstraints = false
    }
    func setupImageView()  {
        self.view.addSubview(mImageView)
        setupImageViewCommon(imageViewSub: mImageView, namePicture: "Arrow.jpg")
        let ratio = NSLayoutConstraint(item: mImageView, attribute: .width, relatedBy: .equal, toItem: mImageView, attribute: .height, multiplier: 16/9, constant: 0)
        let top = NSLayoutConstraint(item: mImageView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant:64)
        let leading = NSLayoutConstraint(item: mImageView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 20)
        let trailing = NSLayoutConstraint(item: mImageView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -20)
        ratio.isActive = true
        top.isActive = true
        leading.isActive = true
        trailing.isActive = true
    }
    
    func setupImageLogo()  {
        self.view.addSubview(mImageLogo)
        setupImageViewCommon(imageViewSub: mImageLogo, namePicture: "logo6.png")
        let ratio = NSLayoutConstraint(item: mImageLogo, attribute: .width, relatedBy: .equal, toItem: mImageLogo, attribute: .height, multiplier: 16/9, constant: 0)
        let leading = NSLayoutConstraint(item: mImageLogo, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 72)
        let trailing = NSLayoutConstraint(item: mImageLogo, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -72)
        let bottom = NSLayoutConstraint(item: mImageLogo, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: -72)
        ratio.isActive = true
        leading.isActive = true
        trailing.isActive = true
        bottom.isActive = true
    }
    func setupMidView()  {
        self.view.addSubview(mMidView)
        mMidView.translatesAutoresizingMaskIntoConstraints = false
        let top = NSLayoutConstraint(item: mMidView, attribute: .top, relatedBy: .equal, toItem: mImageView, attribute: .bottom, multiplier: 1, constant: 20)
        let bottom = NSLayoutConstraint(item: mMidView, attribute: .bottom, relatedBy: .equal, toItem: mImageLogo, attribute: .top, multiplier: 1, constant: -20)
        let leading = NSLayoutConstraint(item: mMidView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 20)
        let trailing = NSLayoutConstraint(item: mMidView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -20)
        top.isActive = true
        bottom.isActive = true
        leading.isActive = true
        trailing.isActive = true
    }
    func setupLabelName()  {
        setupCommonText(subView: mName, text: "Họ Tên:", fontSize: 20, textColor: UIColor.black, superView: mMidView,fontBold:true)
        let top = NSLayoutConstraint(item: mName, attribute: .top, relatedBy: .equal, toItem: mMidView, attribute: .top, multiplier: 1, constant:8)
        let leading = NSLayoutConstraint(item: mName, attribute: .leading, relatedBy: .equal, toItem: mMidView, attribute: .leading, multiplier: 1, constant: 0)
        top.isActive = true
        leading.isActive = true    }
    func setupLabelDesName()  {
        setupCommonText(subView: mDesName, text: "Phan Trung Tính", fontSize: 20, textColor: UIColor.lightGray, superView: mMidView, fontBold: false)
        let top = NSLayoutConstraint(item: mDesName, attribute: .top, relatedBy: .equal, toItem: mMidView, attribute: .top, multiplier: 1, constant:8)
        let leading = NSLayoutConstraint(item: mDesName, attribute: .leading, relatedBy: .equal, toItem: mName, attribute: .trailing, multiplier: 1, constant: 32)
        let vertical  = NSLayoutConstraint(item: mDesName, attribute: .centerY, relatedBy: .equal, toItem: mName, attribute: .centerY, multiplier: 1, constant: 0)
        vertical.isActive = true
        top.isActive = true
        leading.isActive = true
    }
    func setupLabelClass() {
        setupCommonText(subView: mClass, text: "Lớp:", fontSize: 20, textColor: UIColor.black, superView: mMidView, fontBold: true)
        let top = NSLayoutConstraint(item: mClass, attribute: .top, relatedBy: .equal, toItem: mName, attribute: .bottom, multiplier: 1, constant:8)
        let leading = NSLayoutConstraint(item: mClass, attribute: .leading, relatedBy: .equal, toItem: mMidView, attribute: .leading, multiplier: 1, constant: 0)
        top.isActive = true
        leading.isActive = true
    }
    func setupLabelId() {
        setupCommonText(subView: mId, text: "MSSV:", fontSize: 20, textColor: .black, superView: mClass, fontBold: true)
        let top = NSLayoutConstraint(item: mId, attribute: .top, relatedBy: .equal, toItem: mClass, attribute: .bottom, multiplier: 1, constant:8)
        let leading = NSLayoutConstraint(item: mId, attribute: .leading, relatedBy: .equal, toItem: mMidView, attribute: .leading, multiplier: 1, constant: 0)
        top.isActive = true
        leading.isActive = true
    }
    func setupLableAddress()  {
        setupCommonText(subView: mAdd, text: "Địa chỉ:", fontSize: 20, textColor: .black, superView: mId, fontBold: true)
        let top = NSLayoutConstraint(item: mAdd, attribute: .top, relatedBy: .equal, toItem: mId, attribute: .bottom, multiplier: 1, constant:8)
        let leading = NSLayoutConstraint(item: mAdd, attribute: .leading, relatedBy: .equal, toItem: mMidView, attribute: .leading, multiplier: 1, constant: 0)
        top.isActive = true
        leading.isActive = true
    }
    func setupLabelDesClass()  {
        setupCommonText(subView: mDesClass, text: "CTK40", fontSize: 20, textColor: UIColor.lightGray, superView: mMidView, fontBold: false)
        let top = NSLayoutConstraint(item: mDesClass, attribute: .top, relatedBy: .equal, toItem: mMidView, attribute: .top, multiplier: 1, constant:8)
        let vertical  = NSLayoutConstraint(item: mDesClass, attribute: .centerY, relatedBy: .equal, toItem: mClass, attribute: .centerY, multiplier: 1, constant: 0)
        let leading = NSLayoutConstraint(item: mDesClass, attribute: .leading, relatedBy: .equal, toItem: mDesName, attribute: .leading, multiplier: 1, constant: 0)
        vertical.isActive = true
        top.isActive = true
        leading.isActive = true
    }
    func setupLabelDesID()  {
        setupCommonText(subView: mDesId, text: "1610227", fontSize: 20, textColor: UIColor.lightGray, superView: mMidView, fontBold: false)
        let top = NSLayoutConstraint(item: mDesId, attribute: .top, relatedBy: .equal, toItem: mMidView, attribute: .top, multiplier: 1, constant:8)
        let vertical  = NSLayoutConstraint(item: mDesId, attribute: .centerY, relatedBy: .equal, toItem: mId, attribute: .centerY, multiplier: 1, constant: 0)
        let leading = NSLayoutConstraint(item: mDesId, attribute: .leading, relatedBy: .equal, toItem: mDesClass, attribute: .leading, multiplier: 1, constant: 0)
        vertical.isActive = true
        top.isActive = true
        leading.isActive = true
    }
    func setupLabelDesAddress()  {
        setupCommonText(subView: mDesAdd, text: "60 Trần Khánh Dư", fontSize: 20, textColor: UIColor.lightGray, superView: mMidView, fontBold: false)
        let top = NSLayoutConstraint(item: mDesAdd, attribute: .top, relatedBy: .equal, toItem: mMidView, attribute: .top, multiplier: 1, constant:8)
        let vertical  = NSLayoutConstraint(item: mDesAdd, attribute: .centerY, relatedBy: .equal, toItem: mAdd, attribute: .centerY, multiplier: 1, constant: 0)
        let leading = NSLayoutConstraint(item: mDesAdd, attribute: .leading, relatedBy: .equal, toItem: mDesId, attribute: .leading, multiplier: 1, constant: 0)
        vertical.isActive = true
        top.isActive = true
        leading.isActive = true
    }
    func setupCommonText(subView:UILabel,text:String,fontSize:CGFloat,textColor:UIColor,superView:UIView,fontBold:Bool) {
        superView.addSubview(subView)
        subView.text=text
        subView.textColor=textColor
        if(fontBold){
            subView.font=UIFont.boldSystemFont(ofSize:fontSize )
        }else{
            subView.font=UIFont.systemFont(ofSize: fontSize)
        }
        subView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    //region -VARS
    let mImageView = UIImageView()
    let mImageLogo = UIImageView()
    let mMidView = UIView()
    let mName = UILabel()
    let mId = UILabel()
    let mAdd = UILabel()
    let mClass = UILabel()
    let mDesName = UILabel()
    let mDesId = UILabel()
    let mDesAdd = UILabel()
    let mDesClass = UILabel()
    
    //endregion
    
}

