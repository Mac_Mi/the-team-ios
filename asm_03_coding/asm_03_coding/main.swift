//
//  main.swift
//  asm_03_coding
//
//  Created by TinhPhan on 6/14/18.
//  Copyright © 2018 TinhPhan. All rights reserved.
//

import Foundation
func tinhTichBaSo(){
print("Tinh tich ba so")
    var numbers = [Int]()
    var result = 1;
    for index in 0...2{
        print("Nhap vao so thu \(index+1) : ")
        let input = Int(readLine(strippingNewline: true)!)
        numbers.insert(input!, at: index)
        result *= numbers[index]
    }
    print("Tich ba so: \(result)" )
}
//tinhTichBaSo()
func tinhTongSoDauSoCuoi(){
    print("Tinh tong hai so dau cuoi ")
    var numbers = [Int]()
    var result = 1;
    for index in 0...2{
        print("Nhap vao so thu \(index+1) : ")
        let input = Int(readLine(strippingNewline: true)!)
        numbers.insert(input!, at: index)
    }
    result = numbers[0]+numbers[2]
    print("Tong hai so dau cuoi: \(result)" )
}
//tinhTongSoDauSoCuoi()
func tinhTichTuMotDenN(){
    print("Tinh tich tu 1 den N")
    var N = 0;
    var result = 1;
    repeat{
        print("Nhap vao so N nho hon 100: ")
        N = Int(readLine(strippingNewline: true)!)!
    }while N>=100
    for i in 1...N{
        result *= i
    }
    print("Result: \(result)")
}
//tinhTichTuMotDenN()
func tinhTongCacSoChanTuKhongDenN(){
    print("Tinh tong cac so chan tu 0 den N")
    var N = 0;
    var result = 0;
    repeat{
        print("Nhap vao so N nho hon 100: ")
        N = Int(readLine(strippingNewline: true)!)!
    }while N>=100
    for i in 0...N{
        if(i % 2 == 0){
            result += i
        }
    }
    print("Result: \(result)")
}
//tinhTongCacSoChanTuKhongDenN()
func tinhTongCacSoChiaHetChoNamTuKhongDenN(){
    print("Tinh tong cac so chia het cho 5 tu 0 den N")
    var N = 0;
    var result = 0;
    repeat{
        print("Nhap vao so N nho hon 100: ")
        N = Int(readLine(strippingNewline: true)!)!
    }while N>=100
    for i in 0...N{
        if(i % 5 == 0){
            result += i
        }
    }
    print("Result: \(result)")
}
//tinhTongCacSoChiaHetChoNamTuKhongDenN()
func checkCharacterCondition(character:String) -> Bool{
     let array = ["A","B","C","D"]
    for index in 0...3{
        if(character == array[index]) {
            return true
        }
    }
    return false
}
func nhapVaoKiTuInRaKiTuInThuong(){
    var input = ""
    repeat{
        print("Nhap vao mot trong 4 ki tu (A, B, C, F): ")
        input = readLine()!
    }while (!checkCharacterCondition(character: input))
    print("Result: \(input.lowercased())")
}
//nhapVaoKiTuInRaKiTuInThuong()
func tinhTongMaASCII(){
    print("Tinh tong ma ASCII cua hai ki tu ")
    var charOne = ""
    var charTwo = " "
    repeat{
        print("Nhap vao ki tu thu nhat trong 4 ki tu (A, B, C, F): ")
        charOne = readLine()!
        print("Nhap vao ki tu thu hai trong 4 ki tu (A, B, C, F): ")
        charTwo = readLine()!
    }while (!checkCharacterCondition(character: charOne) && !checkCharacterCondition(character: charTwo))
    let asciiSum = UInt8(ascii:UnicodeScalar(charOne)!) + UInt8(ascii:UnicodeScalar(charTwo)!)
    print("Tong ma ascii: \(asciiSum )")
}
tinhTongMaASCII()



