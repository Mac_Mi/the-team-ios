//
//  main.swift
//  asm02_coding
//
//  Created by TinhPhan on 6/14/18.
//  Copyright © 2018 TinhPhan. All rights reserved.
//

import Foundation

func khaiBaoBienVoiLet(){
    print("Khai bao bien voi let")
    let valueInt:Int = 9
    print("value int : \(valueInt)")
    let valueFloat:Float = 9.09
    print("value float : \(valueFloat)")
    let valueDouble:Double = 9.09654
    print("value double : \(valueDouble)")
    let valueString:String = "Bryan"
    print("value string : \(valueString)")
    let valueCharacter:Character = "C"
    print("value character:\(valueCharacter)")
}

func tongHaiSo() {
    print("Tinh tong hai so")
    print("Nhap vao so thu nhat: ")
    let numberOne = Int(readLine(strippingNewline: true)!)
    print("Nhap vao so thu hai" )
    let numberTwo = Int(readLine(strippingNewline: true)!)
    print("Result sum:\(numberOne!+numberTwo!)")
}

func thuongHaiSo(){
    print("Tinh thuong hai so")
    print("Nhap vao so thu nhat: ")
    let numberOne = Double(readLine(strippingNewline: true)!)
    print("Nhap vao so thu hai" )
    let numberTwo = Double(readLine(strippingNewline: true)!)
    print("Result div: \(numberOne!/numberTwo!)")
}
func tinhSoDuPhepChia(){
    print("Tinh so du phep chia hai so")
    print("Nhap vao so thu nhat: ")
    let numberOne = Int(readLine(strippingNewline: true)!)
    print("Nhap vao so thu hai" )
    let numberTwo = Int(readLine(strippingNewline: true)!)
    print("Result mod: \(numberOne!/numberTwo!)")
}
func tongChanLeHaiSo(){
    print("Nhap vao so thu nhat: ")
    let numberOne = Int(readLine(strippingNewline: true)!)
    print("Nhap vao so thu hai" )
    let numberTwo = Int(readLine(strippingNewline: true)!)
    if(numberTwo!+numberOne! % 2 == 0){
        print("Tong chan")
    }else{
        print("Tong le")
    }
}
khaiBaoBienVoiLet()
tongHaiSo()
thuongHaiSo()
tinhSoDuPhepChia()
tongChanLeHaiSo()

