//
//  SecondViewController.swift
//  ViewControllerLifeCycle
//
//  Created by TinhPhan on 6/25/18.
//  Copyright © 2018 TinhPhan. All rights reserved.
//

import  UIKit

class SecondViewController: UIViewController {
    override func viewDidLoad() {
        super .viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("View 2 did load")
    }
    override func viewDidAppear(_ animated: Bool) {
        print("View 2 did appear")
    }
    override func viewWillAppear(_ animated: Bool) {
        print("View 2 will appear ")
    }
    override func viewDidDisappear(_ animated: Bool) {
        print("View 2 did disappear")
    }
    override func viewWillDisappear(_ animated: Bool) {
        print("View 2 will disappear")
    }
    @IBAction func backScreenDisTouch(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        present(viewController, animated: true, completion: nil)
    }
    @IBAction func finishDidTouch(_ sender: UIButton) {
        print("finish view 2")
        dismiss(animated: true, completion: nil)
    }
    
}
