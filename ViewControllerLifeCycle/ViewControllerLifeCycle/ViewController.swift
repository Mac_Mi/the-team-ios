//
//  ViewController.swift
//  ViewControllerLifeCycle
//
//  Created by TinhPhan on 6/25/18.
//  Copyright © 2018 TinhPhan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("View 1 did load")
    }
    override func viewWillAppear(_ animated: Bool) {
        print("View 1 will appear ")
    }
    override func viewDidDisappear(_ animated: Bool) {
        print("View 1 did disappear")
    }
    override func viewWillDisappear(_ animated: Bool) {
        print("View 1 will disappear")
    }
    @IBAction func goScreenDidTouch(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "SecondViewController", bundle: nil)
        let secondScrren = storyboard.instantiateViewController(withIdentifier: "SecondViewController") as! SecondViewController
        present(secondScrren, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

