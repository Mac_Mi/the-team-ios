//
//  AppDelegate.swift
//  ViewControllerLifeCycle
//
//  Created by TinhPhan on 6/25/18.
//  Copyright © 2018 TinhPhan. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        print("AppDelegate said: will finish launching")
        return true
    }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
   print("AppDelegate said: did finish launching")
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
      print("AppDelegate said: will jump down background")
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
 print("AppDelegate said: active on background")
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        print("AppDelegate said: Will jump up foreground")
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        print("AppDelegate said: become active like that resume in android")
    }

    func applicationWillTerminate(_ application: UIApplication) {
        print("AppDelegate said: Destroy")
    }


}

