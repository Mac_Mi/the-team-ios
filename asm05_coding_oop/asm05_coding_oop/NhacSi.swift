//
//  NhacSi.swift
//  asm05_coding_oop
//
//  Created by TinhPhan on 6/15/18.
//  Copyright © 2018 TinhPhan. All rights reserved.
//

import Foundation
class  NhacSi {
    private var mTenNhacSi:String
    private var mNamSinh:Int
    private var mEmail:String
    private var mSoDienThoai:String
    init(tenNhacSi:String,namSinh:Int,email:String,soDienThoai:String) {
        self.mTenNhacSi = tenNhacSi
        self.mNamSinh = namSinh
        self.mEmail=email
        self.mSoDienThoai = soDienThoai
    }
    func hienThiThongTin()  {
        print("Ten nhac si: \(mTenNhacSi)")
        print("Nam sinh: \(mNamSinh)")
        print("Email: \(mEmail)")
        print("So dien thoai: \(mSoDienThoai)")
    }
}
extension NhacSi{
    public func tuoi() -> () {
        print("Chua co tuoi")
    }
}
