//
//  CaSi.swift
//  asm05_coding_oop
//
//  Created by TinhPhan on 6/15/18.
//  Copyright © 2018 TinhPhan. All rights reserved.
//

import Foundation
class CaSi {

    private var  mTenCaSi:String
    private var mNamSinh:Int
    private var mBietDanh:String
    private var mSoDienThoai:String
    private var mEmail: String
    private var mDiaChi:String
    private var mCongTyDaiDien:String

    init(tenCaSi:String , namSinh:Int, bietDanh:String , soDienThoai:String, email:String,diaChi:String,congTyDaiDien:String) {
        self.mTenCaSi = tenCaSi
        self.mNamSinh = namSinh
        self.mBietDanh = bietDanh
        self.mSoDienThoai = soDienThoai
        self.mEmail = email
        self.mDiaChi = diaChi
        self.mCongTyDaiDien = congTyDaiDien
    }

    
    func thongTinCaNhan()  {
        print("Ca si: \(mTenCaSi)")
        print("Nam sinh: \(mNamSinh)")
        print("Biet danh: \(mBietDanh)")
        print("So dien thoai: \(mSoDienThoai)")
        print("Email: \(mEmail)")
        print("Dia chi: \(mDiaChi)")
        print("Cong ty dai dien: \(mCongTyDaiDien)")
    }
    func diDien( diachi:String,thoigian:String)  {
        print("Ca si \(mTenCaSi) di dien o: \(diachi) vao luc :\(thoigian)")
    }
    func diMuaSam(thoiGian:String,sieuThi:String,nguoiDiCung:String)  {
        print("Ca si \(mTenCaSi) di mua sam o: \(sieuThi) vao luc :\(thoiGian) ")
        if(nguoiDiCung != "Khong"){
            print("cung: \(nguoiDiCung)")
        }else{
            print("di mot minh")
        }
    }
    func sangTacBaiHat(tenBaiHat:String,daHoanThanh:Bool)  {
        print("Ca si \(mTenCaSi) sang tac bai hat: \(tenBaiHat)")
        if(daHoanThanh){
            print("Da hoan thanh")
        }else{
            print("Chua hoan thanh")
        }
    }
}
