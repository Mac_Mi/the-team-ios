//
//  BaiHat.swift
//  asm05_coding_oop
//
//  Created by TinhPhan on 6/15/18.
//  Copyright © 2018 TinhPhan. All rights reserved.
//

import Foundation
class BaiHat {
    private var mTenBaiHat:String
    private var mNhacSi:NhacSi
    private var mCaSi:CaSi
    private var mTheLoai:String
    private var mThoiGianSangTac:String
    private var mHopAm:String
    private var mLoiBaiHat:String
  
    init(tenBaiHat:String,nhacSi:NhacSi,caSi:CaSi,theLoai:String,thoiGianSangTac:String,vongHopAm:String,loiBaiHat:String) {
        self.mTenBaiHat=tenBaiHat
        self.mNhacSi = nhacSi
        self.mCaSi = caSi
        self.mThoiGianSangTac = thoiGianSangTac
        self.mHopAm = vongHopAm
        self.mLoiBaiHat = loiBaiHat
        self.mTheLoai = theLoai
    }
    func hienThiThongTin()  {
        print("Ten bai hat: \(mTenBaiHat)")
        mNhacSi.hienThiThongTin()
        mCaSi.thongTinCaNhan()
        print("Thoi gian sang tac: \(mThoiGianSangTac)")
        print("Vong hoa am: \(mHopAm)")
        print("Loi bai hat: \(mLoiBaiHat)")
        print("The loai: \(mTheLoai)")
    }
}
